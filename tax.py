#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
import logging
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


HAS_VATNUMBER = False
try:
    import vatnumber
    HAS_VATNUMBER = True
except ImportError:
    logging.getLogger('account_tax_recapitulative_statement').warning(
            'Unable to import vatnumber. VAT number validation disabled.')

class TaxTemplate(ModelSQL, ModelView):
    _name = 'account.tax.template'

    vat_code_required = fields.Boolean('Vat Code Required',
            readonly=True)

    def _get_tax_value(self, template, tax=None):
        '''
        Set values for tax creation.

        :param template: the BrowseRecord of the template
        :param tax: the BrowseRecord of the tax to update
        :return: a dictionary with account fields as key and values as value
        '''
        res = super(TaxTemplate, self)._get_tax_value(template, tax=tax)
        if not tax or (tax.vat_code_required !=
                template.vat_code_required):
            res['vat_code_required'] = template.vat_code_required
        return res

TaxTemplate()


class Tax(ModelSQL, ModelView):
    _name = 'account.tax'

    vat_code_required = fields.Boolean('Vat Code Required')

    def __init__(self):
        super(Tax, self).__init__()
        self._constraints += [
            ('check_vat_code_required', 'all_vat_code_required'),
            ]
        self._error_messages.update({
            'all_vat_code_required': 'All child taxes must be either '
            'set to vat code required or not!',
            })

    def check_vat_code_required(self, ids):
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = True
        for tax in self.browse(ids):
            if tax.parent:
                if (tax.parent.vat_code_required !=
                        tax.vat_code_required):
                    res = False
                    break
                if tax.parent.parent:
                    if not self.check_vat_code_required(tax.parent.id):
                        res = False
                        break
            if tax.childs:
                for child in tax.childs:
                    if not self.check_vat_code_required(child.id):
                        res = False
                        break
        return res

Tax()


class Line(ModelSQL, ModelView):
    _name = 'account.tax.line'

    vat_code = fields.Char('VAT Code',
            #states={
            #    'required': 'bool(tax.vat_code_required)'
            #    }
            )

    def __init__(self):
        super(Line, self).__init__()
        self._constraints += [
            ('check_vat', 'invalid_vat'),
            ('check_vat_code_required', 'missing_vat'),
        ]
        self._error_messages.update({
            'invalid_vat': 'Invalid VAT number!',
            'no_party_move_line': 'Please set a party on move line!',
            'no_vat_code': 'The party needs a vat code set for usage '
                    'of this tax. Please set a vat code on party!',
            'missing_vat': 'Vat Code missing on tax line with tax that '
                    'required a vat code'
        })
        self.tax = copy.copy(self.tax)
        if not self.tax.on_change:
            self.tax.on_change = []
        if not 'move_line' in self.tax.on_change:
            self.tax.on_change += ['move_line']
        self._reset_columns()

    def on_change_tax(self, vals):
        tax_obj = Pool().get('account.tax')
        move_line_obj = Pool().get('account.move.line')
        res = super(Line, self).on_change_tax(ids, vals)
        if vals.get('tax'):
            tax = tax_obj.browse(vals['tax'])
            if tax.vat_code_required:
                if vals.get('move_line'):
                    move_line = move_line_obj.browse(vals['move_line'])
                    if move_line.party and move_line.party.vat_code:
                        res['vat_code'] = move_line.party.vat_code
        elif vals.has_key('tax'):
            res['vat_code'] = False
        return res

    def create(self, vals):
        tax_obj = Pool().get('account.tax')
        move_line_obj = Pool().get('account.move.line')

        if vals.get('tax'):
            tax = tax_obj.browse(vals['tax'])
            if tax.vat_code_required and not vals.get('vat_code'):
                move_line = move_line_obj.browse(vals['move_line'])
                if not move_line.party:
                    self.raise_user_error('no_party_move_line')
                if not move_line.party.vat_code:
                    self.raise_user_error('no_vat_code')
                vals['vat_code'] = move_line.party.vat_code or False
        return super(Line, self).create(vals)

    def write(self, ids, vals):
        if vals.has_key('tax'):
            vals['vat_code'] = False
            if vals.get('tax'):
                vals2 = vals.copy()
                for line in self.browse(ids):
                    if (line.tax.vat_code_required and
                            not vals.get('vat_code')):
                        if not line.move_line.party:
                            self.raise_user_error('no_party_move_line')
                        if not line.move_line.party.vat_code:
                            self.raise_user_error('no_vat_code')
                        vals2['vat_code'] = move_line.party.vat_code or False
                        self.write(line.id, vals2)
        return super(Line, self).write(ids, vals)

    def check_vat_code_required(self, ids):
        for line in self.browse(ids):
            if line.tax.vat_code_required and not line.vat_code:
                return False
        return True

    def check_vat(self, ids):
        '''
        Check the VAT number depending of the country.
        http://sima-pc.com/nif.php
        '''
        if not HAS_VATNUMBER:
            return True
        for line in self.browse(ids):
            if line.vat_code:
                vat_country = line.vat_code[:2]
                vat_number = line.vat_code[2:]

                if not getattr(vatnumber, 'check_vat_' + \
                        vat_country.lower())(vat_number):
                    return False
        return True

Line()
