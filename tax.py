# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.i18n import gettext
from trytond.model import fields
from trytond.model.exceptions import RequiredValidationError
from trytond.pool import PoolMeta


class TaxTemplate(metaclass=PoolMeta):
    __name__ = 'account.tax.template'

    vat_code_required = fields.Boolean('Vat Code Required')

    def _get_tax_value(self, tax=None):
        res = super(TaxTemplate, self)._get_tax_value(tax=tax)
        field = 'vat_code_required'
        if not tax or getattr(tax, field) != getattr(self, field):
            res[field] = getattr(self, field)
        return res

    @classmethod
    def validate(cls, taxes):
        super(TaxTemplate, cls).validate(taxes)
        for tax in taxes:
            tax.check_vat_code_required()

    def check_vat_code_required(self):
        taxes = self.search([
                ('vat_code_required', '!=', self.vat_code_required),
                ['OR',
                    ('id', 'in', [t.id for t in self.childs]),
                    ('parent', '=', self.id)
                    ],
                ])
        if taxes:
            raise RequiredValidationError(gettext(
                    'account_tax_recapitulative_statement.'
                    'same_vat_code_required',
                    first=self.rec_name, second=taxes[0].rec_name,))


class Tax(metaclass=PoolMeta):
    __name__ = 'account.tax'

    vat_code_required = fields.Boolean('Vat Code Required')


class Line(metaclass=PoolMeta):
    __name__ = 'account.tax.line'

    vat_code = fields.Char('VAT Code')

    @fields.depends('tax', 'move_line', '_parent_move_line.id')
    def on_change_tax(self):
        try:
            super().on_change_tax()
        except:
            pass

        self.vat_code = None
        if self.tax:
            if self.tax.vat_code_required:
                if (self.move_line.party
                        and self.move_line.party.tax_identifier):
                    self.vat_code = self.move_line.party.tax_identifier.code

    @classmethod
    def validate(cls, lines):
        super(Line, cls).validate(lines)
        for line in lines:
            line.check_vat_code()

    def check_vat_code(self):
        if self.tax.vat_code_required and not self.vat_code:
            raise RequiredValidationError(gettext(
                    'account_tax_recapitulative_statement.'
                    'missing_line_vat_code',
                    line=self.id))
            self.move_line.party.check_vat()

    @classmethod
    def _set_vat_code(cls, lines):
        '''
        Check and set the vat code according to the used taxes.
        '''
        for line in lines:
            if line.tax:
                if line.tax.vat_code_required and not line.vat_code:
                    if not line.move_line.party:
                        raise RequiredValidationError(gettext(
                                'account_tax_recapitulative_statement.'
                                'missing_party_move_line'))
                    if not line.move_line.party.tax_identifier:
                        raise RequiredValidationError(gettext(
                                'account_tax_recapitulative_statement.'
                                'missing_party_vat_code',
                                party=line.move_line.party.rec_name))
                    line.move_line.party.check_vat()
                    vat_code = line.move_line.party.tax_identifier.code
                    cls.write([line.id], {
                            'vat_code': vat_code,
                            })

    @classmethod
    def create(cls, vlist):
        lines = super(Line, cls).create(vlist)
        cls._set_vat_code(lines)
        return lines

    @classmethod
    def write(cls, *args):
        super(Line, cls).write(*args)
        lines = sum(args[::2], [])
        cls._set_vat_code(lines)
