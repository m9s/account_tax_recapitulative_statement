# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Tax Recapitulative Statement',
    'name_de_DE': 'Buchhaltung Steuern Zusammenfassende Meldung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Recapitulative Statement
    - Provides the base for the preparation of Recapitulative Statements.
''',
    'description_de_DE': '''Zusammenfassende Meldung
    - Stellt die Grundlagen für die Erstellung von Zusammenfassenden Meldungen
      (ZM) bereit.
''',
    'depends': [
        'account'
    ],
    'xml': [
        'tax.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
